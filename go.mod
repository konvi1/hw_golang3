module gitlab.com/konvi1/hw_golang3

go 1.18

require (
	gitlab.com/konvi1/libtextfile v0.0.4
	gopkg.in/yaml.v2 v2.4.0
)
