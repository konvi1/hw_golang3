package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/konvi1/libtextfile"
	"gopkg.in/yaml.v2"
)

//import "libtextfile"

/*
Написать приложение, которое использует эту библиотеку
Если при запуске передан флаг line, то приложение должно выводить на экран количество строк в текстовом файле, если флаг word,
то выводить на экран количество слов в текстовом файле. Оба флага могут быть указаны одновременно.

Название директории и название файла должны читаться основным приложением из конфигурационного yaml-файла.
Сам файл конфигураций должен задаваться ключем -c. Если ключ не указан, то название конфигурационного
файла должно читаться из переменных сред.

При выполнении задания необходимо использовать go-модули.
*/

/*
примеры команд

main.exe -line -c config.yaml -- кол-во строк

main.exe -word -c config.yaml -- кол-во слов

main.exe -line  -- название конфигурационного файла должно читаться из переменных сред

*/

// структура конфиг. файла
type config struct {
	Pathfile string `yaml:"pathfile"`
	File     string `yaml:"file"`
}

func main() {
	// Читаем флаги
	flagLine := flag.Bool("line", false, "enter flag line")

	flagWord := flag.Bool("word", false, "enter flag word")

	var vConfigFile string
	flag.StringVar(&vConfigFile, "c", "", "enter flag config file")
	flag.Parse()

	if strings.HasPrefix(vConfigFile, "-") {
		fmt.Println("ERROR flag needs an argument: -c")
		return
	}

	// если параметра нет, то название конфигурационного файла должно читаться из переменных сред
	if vConfigFile == "" {
		v, exist := os.LookupEnv("GODIVECONFIG")
		if !exist {
			fmt.Println("ERROR non exist var GODIVECONFIG")
			return
		}
		vConfigFile = v
	}

	// Файл конфигурации
	var cnf config
	data, err := ioutil.ReadFile(vConfigFile)
	if err != nil {
		fmt.Println("ERROR read file:", vConfigFile)
		return
	}

	err = yaml.Unmarshal(data, &cnf)
	if err != nil {
		fmt.Println(err)
		return
	}

	if *flagLine {
		// делать подсчет строк в файле
		vCountLine, err := libtextfile.CountLineInFile(cnf.Pathfile + cnf.File)
		if err != nil {
			return
		}
		fmt.Println("Count of lines:", vCountLine)
	}

	if *flagWord {
		// делать подсчет слов в файле
		vCountWord, err := libtextfile.CountWordInFile(cnf.Pathfile + cnf.File)
		if err != nil {
			return
		}
		fmt.Println("Count of words:", vCountWord)
	}

}
